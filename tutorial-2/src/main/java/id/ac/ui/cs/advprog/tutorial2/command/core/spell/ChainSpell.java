package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

public class ChainSpell implements Spell {
    private ArrayList<Spell> spells;
    private Stack<Spell> doneSpell;

    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
        doneSpell = new Stack<Spell>();
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        for(Spell s: this.spells){
            s.cast();
            doneSpell.push(s);
        }
    }

    @Override
    public void undo() {
        while(!doneSpell.isEmpty()){
            Spell s = doneSpell.pop();
            s.cast();
        }
    }
}
