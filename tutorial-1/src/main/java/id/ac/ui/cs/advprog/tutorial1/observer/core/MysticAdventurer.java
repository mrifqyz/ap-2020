package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.guild = guild;
                this.name = "Mystic";
                guild.add(this);
        }

        @Override
        public void update() {
                if(guild.getQuestType().equals("D") || guild.getQuestType().equals("E"))
                        this.getQuests().add(guild.getQuest());
        }
}
