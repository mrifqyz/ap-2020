package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    private String alias;

    public MysticAdventurer(){
        this.alias = "Mystic";
        attackBehavior = new AttackWithMagic();
        defenseBehavior = new DefendWithShield();
    }

    @Override
    public String getAlias() {
        return alias;
    }
}
